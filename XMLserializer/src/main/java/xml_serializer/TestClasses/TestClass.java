package ru.yandex.skorzanydran.xml_serializer.TestClasses;

import ru.yandex.skorzanydran.xml_serializer.Annotations.XmlObject;
import ru.yandex.skorzanydran.xml_serializer.Annotations.XmlTag;

@XmlObject
public class TestClass extends Person{

    @XmlTag
    private String object;

    public TestClass(String object){
        super("name", "sname", "lang", 12, 123);
        this.object = object;
    }
}
