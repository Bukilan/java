package ru.yandex.skorzanydran.xml_serializer;

import ru.yandex.skorzanydran.xml_serializer.XMLObjects.Sk_Tag;

import java.lang.reflect.InvocationTargetException;


interface ObjectParser {
    Sk_Tag parse(Object obj) throws IllegalAccessException, InvocationTargetException;
}
