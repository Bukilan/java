package ru.yandex.skorzanydran.xml_serializer.Annotations;


import java.lang.annotation.*;

@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface XmlObject {}
