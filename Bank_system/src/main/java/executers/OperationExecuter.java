package ru.yandex.skorzanydran.executers;

import ru.yandex.skorzanydran.operations.Operation;

public abstract class OperationExecuter {

    protected final OperationExecuter next;

    public OperationExecuter(OperationExecuter operationExecuter){
        next = operationExecuter;
    }

    public abstract void executeOperation(Operation operation);
}
