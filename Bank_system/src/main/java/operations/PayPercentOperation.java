package ru.yandex.skorzanydran.operations;

import ru.yandex.skorzanydran.accounts.Account;
import ru.yandex.skorzanydran.accounts.BaseAccount;

public class PayPercentOperation implements Operation {

    protected Account account;

    public PayPercentOperation(Account account){
        this.account = account;
    }

    public void execute() {
        account.payPercents();
    }
}
