package ru.yandex.skorzanydran.operations;

import ru.yandex.skorzanydran.accounts.Account;

import java.util.Date;

public class SubstractCommissionOperation implements Operation{

    protected Account account;

    public SubstractCommissionOperation(Account account) throws Exception {
        this.account = account;
    }


    public void execute() {
        account.substractCommissions();
    }
}
