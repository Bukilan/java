package ru.yandex.skorzanydran.operations;

public interface Operation {
    void execute();
}
