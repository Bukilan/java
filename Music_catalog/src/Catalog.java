import java.io.OutputStream;
import java.util.*;
import java.util.function.Function;

public class Catalog{
    protected enum Parameter{
        NAME(Song::getName),
        AUTHOR(Song::getAuthor),
        ALBUM(Song::getAlbum),
        GENRE(Song::getGenre),
        DATE(Song::getDate),
        COLL(Song::getCollection);

        private final Function<Song, String> extractor;

        Parameter(Function<Song, String> extractor) {
            this.extractor = extractor;
        }

        public Function<Song, String> getExtractor() {
            return extractor;
        }
    }
    private EnumMap<Parameter, HashMap<String, Set<Song>>> indexes = new EnumMap<>(Parameter.class);
    protected Set<Song> catalog;
    public Tree genres;
    private Search search;

    Catalog(){
        catalog = new HashSet<>();
        search = new Search();
        genres = new Tree();
    }

    public void addGenre(String name){
        genres.addGenre(name);
    }

    public void addGenre(String name, String parentName){
        genres.addGenre(name, parentName);
    }

    public boolean findGenre(String name){
        return genres.findGenre(name) != null;
    }

    private void addToIndexes(Song song){
        Arrays.stream(Parameter.values())
                .forEach(p -> getIndex(p).computeIfAbsent(p.extractor.apply(song), k -> new HashSet<>())
                        .add(song));
        Tree.Node node = new Tree.Node(genres.findGenre(song.getGenre()));
        node = node.parent;
        List<String> p = new LinkedList<>();
        while(node != null){
            p.add(node.genre);
            node = node.parent;
        }
        Arrays.stream(p.toArray()).forEach(s -> getIndex(Parameter.GENRE).computeIfAbsent((String) s, k -> new HashSet<>()).add(song));
    }

    public void addSong(Song song){
        catalog.add(song);
        addToIndexes(song);
    }

    protected Map<String, Set<Song>> getIndex(Parameter p){
        return indexes.computeIfAbsent(p, k -> new HashMap<>());
    }

    public void find(String command, OutputStream out){
        search.find(command, this, out);
    }
}
