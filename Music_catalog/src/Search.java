import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Set;

import static java.lang.System.nanoTime;

public class Search {
    private static final String s_NAME = "-n";
    private static final String s_AUTHOR = "-a";
    private static final String s_ALBUM = "-A";
    private static final String s_GENRE = "-g";
    private static final String s_DATE = "-d";
    private static final String s_COLL = "-c";
    private double time;
    private EnumMap<Catalog.Parameter, String> parseCommand(String command) throws IllegalArgumentException{
        EnumMap<Catalog.Parameter, String> attr = new EnumMap<Catalog.Parameter, String>(Catalog.Parameter.class);
        String[] cmd = command.split(" ", 0);
        if (cmd[0].equals("find")){
            for (int i = 1; i < cmd.length; i++){
                if (cmd[i].equals(s_NAME)){
                    attr.put(Catalog.Parameter.NAME, cmd[i + 1]);
                    i++;
                }
                else if (cmd[i].equals(s_AUTHOR)){
                    attr.put(Catalog.Parameter.AUTHOR, cmd[i + 1]);
                    i++;
                }
                else if (cmd[i].equals(s_ALBUM)){
                    attr.put(Catalog.Parameter.ALBUM, cmd[i + 1]);
                    i++;
                }
                else if (cmd[i].equals(s_GENRE)){
                    attr.put(Catalog.Parameter.GENRE, cmd[i + 1]);
                    i++;
                }
                else if (cmd[i].equals(s_DATE)){
                    attr.put(Catalog.Parameter.DATE, cmd[i + 1]);
                    i++;
                }
                else if (cmd[i].equals(s_COLL)){
                    attr.put(Catalog.Parameter.COLL, cmd[i + 1]);
                    i++;
                }
                else throw new IllegalArgumentException("Bad command: " + command);
            }
        }
        return attr;
    }

    private void findColl(Catalog catalog, Catalog.Parameter p, Set<Song> result, EnumMap<Catalog.Parameter, String> attr){
        if(attr.get(p) != null) {
            if(catalog.getIndex(p).get(attr.get(p)) != null) {
                result.retainAll(catalog.getIndex(p).get(attr.get(p)));
            }
            else result.clear();
        }
    }

    private Set<Song> search(Catalog catalog, EnumMap<Catalog.Parameter, String> attr){
        Set<Song> result = new HashSet<>(catalog.catalog);
        time = nanoTime();
        Arrays.stream(Catalog.Parameter.values()).forEach(p -> findColl(catalog, p, result, attr));
        time = (((nanoTime() - time)) / 1000000000);
        return result;
    }

    protected void find(String command, Catalog catalog, OutputStream out){ // перенести в мейн (типа вывод в мейне с передачей result)
        try {
            EnumMap<Catalog.Parameter, String> attr = parseCommand(command);
            Set<Song> set = search(catalog, attr);
            double t = nanoTime();
            for (Song s: set) {
                System.out.println(s.getAuthor()+"-"+s.getName()+" ("+s.getAlbum()+") "+" / "+s.getGenre());
            }
            System.out.println("The printout was completed in: " + ((double)nanoTime() - t) / 1000000000 + " seconds");
            System.out.println("The search was completed in: " + time + " seconds");
        }
        catch(IllegalArgumentException e){
            System.out.println(e);
        }
    }
}


