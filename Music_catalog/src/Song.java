import java.util.Objects;

public class Song {
    private final String name;
    private final String author;
    private final String album;
    private final String genre;
    private final String date;
    private final String collection;

    public Song(String name, String author, String album, String genre, String date, String collection) {
        this.name = name;
        this.author = author;
        this.album = album;
        this.genre = genre;
        this.date = date;
        this.collection = collection;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return Objects.equals(name, song.name) &&
                Objects.equals(author, song.author) &&
                Objects.equals(album, song.album) &&
                Objects.equals(genre, song.genre) &&
                Objects.equals(date, song.date);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, author, album, genre, date);
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getAlbum() {
        return album;
    }

    public String getGenre() {
        return genre;
    }

    public String getDate() {
        return date;
    }

    public String getCollection(){
        return collection;
    }
}
