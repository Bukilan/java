import java.io.*;

import static java.lang.System.nanoTime;


public class Main {
    public static void main(String[] argc) throws IOException {
        Catalog catalog = new Catalog();
        for(int i = 0; i < 10; i++){
            catalog.addGenre("genre_" + i);
        }
        int j = 0;
        for(int i = 10; i < 30; i+=2){
            catalog.addGenre("genre_" + i, "genre_" + j);
            catalog.addGenre("genre_" + (i + 1), "genre_" + j);
            j++;
        }

        for (int i = 0; i < 1000000; i++) {
            String name = "name_" + i;
            String author = "author_" + i % 100000;
            String album = "album_" + (i % 100000 + i % 10000);
            String genre = "genre_" + (i % 30);
            String date = "date_" + (2000 + (i % 100000));
            String collection = "coll_" + (i % 100000);
            catalog.addSong(new Song(name, author, album, genre, date, collection));
        }

        BufferedReader fin = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = fin.readLine()) != null) {
            double time = nanoTime();
            catalog.find(line, System.out);
            System.out.println("The request was completed in: " + ((double)nanoTime() - time) / 1000000000 + " seconds");
        }
    }
}
