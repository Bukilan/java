import java.util.*;

public class Tree {
    HashMap<String, Node> tree;
    protected static class Node{
        protected String genre;
        protected Node parent;

        Node(String genreName){
            genre = genreName;
            parent = null;
        }

        public Node(Node node) {
            genre = node.genre;
            parent = node.parent;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return Objects.equals(genre, node.genre) &&
                    Objects.equals(parent, node.parent);
        }

        @Override
        public int hashCode() {

            return Objects.hash(genre, parent);
        }
    }

    Tree(){
        tree = new HashMap<>();
    }

    public void addGenre(String name) throws IllegalArgumentException{
        Node n = new Node(name);
        if(n.equals(tree.get(name))){
            throw new IllegalArgumentException("this genre almost exist");
        }
        else tree.put(name, n);
    }

    public void addGenre(String name, String parentName) throws IllegalArgumentException{
        Node n = new Node(name);
        if(n.equals(tree.get(name))){
            throw new IllegalArgumentException("this genre almost exist");
        }
        else if(tree.get(parentName) == null){
            throw new IllegalArgumentException("there is no genre: " + parentName);
        }
        else {
            n.parent = tree.get(parentName);
            tree.put(name, n);
        }
    }

    public Node getParent(Node n){
        return n.parent;
    }

    public Node findGenre(String name){
        return tree.get(name);
    }
}
