class Fraction implements Comparable<Fraction>  {
    private final int numerator;
    private final int denominator;

    Fraction(int n, int m){
        numerator = n;
        denominator = m;
    }

    public int getNumerator(){
        return numerator;
    }

    public int getDenominator(){
        return denominator;
    }

    public Fraction plus(Fraction other){
        long numer = (long) numerator * other.denominator + (long) denominator * other.numerator;
        long denom = (long) denominator * other.denominator;
        long min = Math.min(numer, denom);
        for(long i = 2; i < min; i++){
            if(numer % i == 0 && denom % i == 0) {
                numer /= i;
                denom /= i;
            }
        }
        return new Fraction((int) numer, (int) denom);
    }

    @Override
    public int compareTo(Fraction o) {
        return 0;
    }
}
