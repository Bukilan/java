import java.util.LinkedList;
import java.util.List;
import java.util.Collections;


public class Polynomial {
    private List<Fraction> polinom;

    Polynomial(FractionSet set){
        polinom = set.getFractions();
    }

    private Polynomial(){
        polinom = new LinkedList<>();
    }

    public Polynomial sum(Polynomial other){
        final int delta = Math.abs(polinom.size() - other.polinom.size());
        Fraction[] arr = polinom.size() > other.polinom.size() ? polinom.toArray(new Fraction[0]) : other.polinom.toArray(new Fraction[0]);
        Fraction[] otherArr = polinom.size() > other.polinom.size() ? other.polinom.toArray(new Fraction[0]) : polinom.toArray(new Fraction[0]);
        for(int i = 0; i < otherArr.length; i++){
            arr[i + delta] = arr[i + delta].plus(otherArr[i]);
        }
        Polynomial pol = new Polynomial();
        Collections.addAll(pol.polinom, arr);
        return pol;
    }

//    public void print() {
//        Fraction[] arr = polinom.toArray(new Fraction[0]);
//        for (int i = 0; i < arr.length; i++) {
//            if (arr.length - i - 1 == 1) {
//                System.out.print("(" + arr[i].getNumerator() + "/" + arr[i].getDenominator() + ")x" + " + ");
//            } else if (arr.length - i - 1 == 0) {
//                System.out.print(arr[i].getNumerator() + "/" + arr[i].getDenominator());
//            } else
//                System.out.print("(" + arr[i].getNumerator() + "/" + arr[i].getDenominator() + ")x^" + (arr.length - i - 1) + " + ");
//        }
//        System.out.println();
//
//    }

    @Override
    public String toString() {
        Fraction[] arr = polinom.toArray(new Fraction[0]);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            if (arr.length - i - 1 == 1) {
                sb.append("(" + arr[i].getNumerator() + "/" + arr[i].getDenominator() + ")x" + " + ").append("\n");
            } else if (arr.length - i - 1 == 0) {
                sb.append(arr[i].getNumerator() + "/" + arr[i].getDenominator()).append("\n");
            } else
                sb.append("(" + arr[i].getNumerator() + "/" + arr[i].getDenominator() + ")x^" + (arr.length - i - 1) + " + ").append("\n");
        }
        return sb.toString();
    }
}

// string builder метод tostring , потом polynom.tostring в мейне и принтим