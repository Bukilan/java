public class Main {
    public static void main(String[] argc){
        FractionSet collection = new FractionSet();

        collection.addFraction(1,3);
        collection.addFraction(1,5);
        collection.addFraction(1,100);
        collection.addFraction(1,1);
        collection.addFraction(1,4);

        System.out.println(collection.toString());

        System.out.print("amount of fractions bigger than 1/200: " + collection.moreThan(1,200));
        System.out.println("amount of fractions, which is less, than 1/3 is: " + collection.lessThan(1,3));

        Polynomial polinom = new Polynomial(collection);

        FractionSet set = new FractionSet();

        set.addFraction(2,1);
        set.addFraction(3,1);

        Polynomial polinomNew = new Polynomial(set);

        System.out.print(polinom);
        System.out.print(polinomNew);

        polinom = polinom.sum(polinomNew);

    }
}
