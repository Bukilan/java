import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FractionSet {
    private List<Fraction> fractions = new ArrayList<Fraction>();

    private void addFraction(Fraction fraction){
        this.fractions.add(fraction);
    }

    public void addFraction(int n, int m){
        Fraction fraction = new Fraction(n,m);
        this.addFraction(fraction);
    }

    public Fraction maxFraction(){
        Fraction max = fractions.iterator().next();
        for (Fraction i : fractions) {
            if (max.getNumerator() * i.getDenominator() < i.getNumerator() * max.getDenominator()){
                max = i;
            }
        }
        return max;
    }

    public Fraction minFraction(){
        return Collections.min(fractions);
        Fraction min = fractions.iterator().next();
        for (Fraction i : fractions) {
            if (min.getNumerator() * i.getDenominator() > i.getNumerator() * min.getDenominator()){
                min=i;
            }
        }
        return min;
    }

    private int moreThan(Fraction fraction){
        int counter = 0;
        for (Fraction i : fractions) {
            if (fraction.getNumerator() * i.getDenominator() < i.getNumerator() * fraction.getDenominator()){
                counter++;
            }
        }
        return counter;
    }

    public int moreThan(int n, int m){
        Fraction fraction = new Fraction(n,m);
        return moreThan(fraction);
    }

    private int lessThan(Fraction fraction){
        int counter = 0;
        for (Fraction i : fractions) {
            if (fraction.getNumerator() * i.getDenominator() > i.getNumerator() * fraction.getDenominator()){
                counter++;
            }
        }
        return counter;
    }

    public int lessThan(int n, int m){
        Fraction fraction = new Fraction(n,m);
        return lessThan(fraction);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Fraction i : fractions){
            sb.append(i.getNumerator() + "/" + i.getDenominator() + ", ").append("\n");
        }
        return sb.toString();
    }

    public List<Fraction> getFractions(){
        return fractions;
    }
}
