package Parser;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class INIData {
    private final static Pattern command = Pattern.compile("find (\\w+) (\\w+) from (\\w+)");
    private final static Pattern fileFormat = Pattern.compile("([a-zA-Z]:)?(\\\\[a-zA-Z0-9_.-]+)+\\\\?");
    private final File file;
    private final Processor processor;
    private static final Logger log = Logger.getLogger(INIData.class.getName());

    public INIData(String pathName){
        Matcher matcher = fileFormat.matcher(pathName);
        try {
            if (!matcher.matches()) {
                throw new IllegalArgumentException("Bad file: " + "'" + pathName + "'");
            }
        }
        catch(IllegalArgumentException e){
            log.error("EXCEP", e);
        }
        file = new File(pathName);
        processor = new Processor();
    }

    public void parse(){
        try{
            Parser.parse(file, processor);
        }
        catch(IOException | IllegalArgumentException e){
            log.error("EXCEP", e);
        }
    }

    private Object find(String cmd){
        Matcher matcher = command.matcher(cmd);
        try{
            if(matcher.matches()){
                String value = processor.data.get(matcher.group(3)).get(matcher.group(2));
                if(matcher.group(1).equals("int")){
                    if (Convertor.toInt(value) == null){
                        log.error("EXCEP", new IllegalArgumentException(value + "is not an integer"));
                    }
                    else return Convertor.toInt(value);
                }
                else if(matcher.group(1).equals("double")){
                    if (Convertor.toDouble(value) == null){
                        log.error("EXCEP", new IllegalArgumentException(value + "is not a double"));
                    }
                    else return Convertor.toDouble(value);
                }
                else if(matcher.group(1).equals("string")){
                    return value;
                }
            }
            else throw new IllegalArgumentException("Unknown command: " + cmd);
        }
        catch (IllegalArgumentException e){
            log.error("EXCEP", e);
        }
        return null;
    }

    public Object searching(String stopCommand) throws IOException {
        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(!(line = reader.readLine()).equals(stopCommand)){
            System.out.println(find(line));
        }
        return null;
    }
}

